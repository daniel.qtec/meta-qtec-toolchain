# Move lib* from lib64 to lib
do_install_append() {
       cp -rav ${D}${libdir}64/lib* ${D}${libdir}
       rm -rf ${D}${libdir}64
}
