require recipes-devtools/gcc/gcc-${PV}.inc
require recipes-devtools/gcc/libgcc.inc

TARGET_CC_ARCH += "${LDFLAGS}" 

do_install_append() {
       cp -rav ${D}${libdir}64/lib* ${D}${base_libdir}
       rm -rf ${D}${libdir}64
}
